---
# BBOSS Copyright (C) 2018-2021, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

#   Packaging steps : (package)
#       1. prepare
#    -> 2. upload
#       3. configure
#       4. download

# Tasks processed on localhost
- name: "Upload - Clone {{ project_name | capitalize }} repositories"
  ansible.builtin.git:
    dest: "{{ venv_offline_output }}{{ project_name }}/{{ item.key }}"
    repo: "{{ item.value.repo }}"
    version: "{{ item.value.ref | default(omit, true) }}"
    depth: 1
    recursive: yes
  delegate_to: localhost
  register: _upload_repositories
  loop: "{{ venv_offline_sources | dict2items }}"

- name: "Upload - List {{ project_name | capitalize }} missing archives"
  ansible.builtin.stat:
    path: "{{ venv_offline_output }}{{ project_name }}/{{ item }}.tar.gz"
  delegate_to: localhost
  register: _upload_archives
  loop: "{{ venv_offline_sources.keys() }}"

- name: "Upload - Update {{ project_name | capitalize }} archives"
  community.general.archive:
    dest: "{{ venv_offline_output }}{{ project_name }}/{{ item }}.tar.gz"
    path: "{{ venv_offline_output }}{{ project_name }}/{{ item }}/"
    format: gz
  delegate_to: localhost
  register: _upload_updates
  loop: >
    {{ _upload_repositories |
      community.general.json_query("results[?changed].item.key") |
      union( _upload_archives |
        community.general.json_query("results[?!(stat.exists)].item")) |
        unique | sort
    }}

# Tasks processed on remote host
- name: "Upload - Create {{ project_name | capitalize }} directories"
  ansible.builtin.file:
    path: "{{ project_path }}{{ item }}"
    state: directory
    mode: 0755
  loop: "{{ venv_offline_sources.keys() }}"
  register: _upload_directories

- name: "Upload - Extract {{ project_name | capitalize }} archives"
  ansible.builtin.unarchive:
    src: "{{ venv_offline_output }}{{ project_name }}/{{ item }}.tar.gz"
    dest: "{{ project_path }}{{ item }}"
    remote_src: no
  loop: >
    {{ _upload_updates |
      community.general.json_query("results[?changed].item") |
      union( _upload_directories |
        community.general.json_query("results[?changed].item")) |
        unique | sort
    }}
  register: _offline_package_upload
